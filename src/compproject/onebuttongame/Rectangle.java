package compproject.onebuttongame;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;

public class Rectangle implements RenderNode{

	private static FloatBuffer vertexBuffer;	// buffer holding the vertices
	private static float vertices[] = {
			-0.5f, -0.5f,  0.0f,		// V1 - bottom left
			-0.5f,  0.5f,  0.0f,		// V2 - top left
			0.5f, -0.5f,  0.0f,		// V3 - bottom right
			0.5f,  0.5f,  0.0f			// V4 - top right
	};
	
	private static FloatBuffer textureBuffer;	// buffer holding the texture coordinates
	private static float texture[] = {
			// Mapping coordinates for the vertices
			0.0f, 1.0f,		// top left		(V2)
			0.0f, 0.0f,		// bottom left	(V1)
			1.0f, 1.0f,		// top right	(V4)
			1.0f, 0.0f		// bottom right	(V3)
	};
	
	public float x;
	public float y;
	private float width;
	private float height;
	
	private float realx;
	private float realy;

	private int options;
	
	
	/** The texture pointer */
	private int[] textures = new int[1];
	private int textureID;
	private boolean textureSet;
	
	public void setTextureID(int textureID)
	{
		this.textureID = textureID;
	}

	public float getHeight()
	{
		return height;
	}

	public void setHeight(float height)
	{
		if(height >= 0)
			this.height = height;
	}

	public int getOptions()
	{
		return options;
	}

	public void setOptions(int options)
	{
		this.options = options;
	}

	public float getWidth()
	{
		return width;
	}

	public void setWidth(float width)
	{
		if(width >= 0)
			this.width = width;
	}

	public Rectangle()
	{
		this(0, 0, 1, 1, 0);
	}
	
	public Rectangle(float x, float y, float width, float height)
	{
		this(x, y, width, height, 0);
	}
	
	public Rectangle(float x, float y, float width, float height, int options)
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.options = options;
		textureID = 0;
		textureSet = false; 
		
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(vertices.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		vertexBuffer = byteBuffer.asFloatBuffer();
		vertexBuffer.put(vertices);
		vertexBuffer.position(0);

		byteBuffer = ByteBuffer.allocateDirect(texture.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		textureBuffer = byteBuffer.asFloatBuffer();
		textureBuffer.put(texture);
		textureBuffer.position(0);
	}
	
	public void loadGLTexture(GL10 gl, Context context, int id) {
		// loading texture
		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), id);
		
		// generate one texture pointer
		gl.glGenTextures(1, textures, 0);
		// ...and bind it to our array
		gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0]);

		// create nearest filtered texture
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_NEAREST);
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
		
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_REPEAT);
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_REPEAT);

		// Use Android GLUtils to specify a two-dimensional texture image from our bitmap
		GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);

		// Clean up
		bitmap.recycle();
		
		
		textureSet = true;
	}

	
	private void calcRealCoordinates()
	{
		if((options & RenderNode.X_CENTERED) == RenderNode.X_CENTERED)
			realx = x;
		else
			realx = x+width/2;
		
		if((options & RenderNode.Y_CENTERED) == RenderNode.Y_CENTERED)
			realy = y;
		else
			realy = y+height/2;
			
	}
	
	/** The draw method for the square with the GL context */
	public void draw(GL10 gl)
	{
		if(textureSet == false && textureID != 0)
		{
			loadGLTexture(gl,GameMain.getInstance().getContext(),textureID);
		}
		
		calcRealCoordinates();
		gl.glTranslatef(realx,realy, 0.0f);
		gl.glScalef(width, height, 0.0f);
		
		// bind the previously generated texture
		gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0]);

		
		// Point to our buffers
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

		// Set the face rotation
		gl.glFrontFace(GL10.GL_CW);

		// Point to our vertex buffer
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
		gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, textureBuffer);

		// Draw the vertices as triangle strip
		gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, vertices.length / 3);

		//Disable the client state before leaving
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
	}
}

