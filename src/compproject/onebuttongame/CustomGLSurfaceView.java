package compproject.onebuttongame;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;

public class CustomGLSurfaceView extends GLSurfaceView {

	public CustomGLSurfaceView(Context context)
	{
		super(context);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		
		 switch (e.getAction())
		 {
	        case MotionEvent.ACTION_DOWN:
	        	GameMain.getInstance().getCurrentState().handleTouchingEvents(e);
	        	break;
	        case MotionEvent.ACTION_UP:
	        	GameMain.getInstance().getCurrentState().handleTouchingEvents(e);
	        	break;
		 }
	     return true;
	}

}
