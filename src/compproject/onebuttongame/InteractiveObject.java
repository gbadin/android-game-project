package compproject.onebuttongame;

public class InteractiveObject {

	private float x;
	private float y;
	private float width;
	private float height;
	private Rectangle renderer;
	
	private void updateRenderer()
	{
		if(renderer != null)
		{
			renderer.x = x;
			renderer.y = y;
			renderer.setWidth(width);
			renderer.setHeight(height);
		}
	}
	
	/**
	 * Default constructor : construct an object with all values to 0;
	 */
	public InteractiveObject()
	{
		this(0.0f,0.0f,0.0f,0.0f);
	}
	
	/**
	 * Constructor with all position and size values
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public InteractiveObject(float x, float y, float width, float height)
	{
		renderer = new Rectangle();
		initPosition(x,y);
		initDimensions(width,height);
	}
	
	/**
	 * check if there is a collision with another interactiveObject
	 * @param otherOject the object to test the collision with
	 * @return true if there is a collision
	 */
	public boolean isCollision(InteractiveObject otherOject)
	{
		if(x+width > otherOject.getX()
			&& x < otherOject.getX()+otherOject.getWidth()
			&& y+height > otherOject.getY()
			&& y < otherOject.getY()+otherOject.getHeight())
			return true;
		return false;
	}
	
	/**
	 * Setter for both x and y values
	 * @param x
	 * @param y
	 */
	public void initPosition(float x, float y)
	{
		this.x = x;
		this.y = y;
		updateRenderer();
	}
	
	/**
	 * Setter for both width and height values
	 * @param width
	 * @param height
	 */
	public void initDimensions(float width, float height)
	{
		this.width = width;
		this.height = height;
		updateRenderer();
	}
	
	/**
	 * Getter for x
	 * @return x position
	 */
	public float getX()
	{
		return x;
	}

	/**
	 * setter for x
	 * @param x
	 */
	public void setX(float x)
	{
		this.x = x;
		updateRenderer();
	}

	/**
	 * Getter for y
	 * @return y position
	 */
	public float getY()
	{
		return y;
	}

	/**
	 * Setter for y
	 * @param y
	 */
	public void setY(float y)
	{
		this.y = y;
		updateRenderer();
	}

	/**
	 * Getter for width
	 * @return width
	 */
	public float getWidth()
	{
		return width;
	}

	/**
	 * Setter for width
	 * @param width
	 */
	public void setWidth(float width)
	{
		this.width = width;
		updateRenderer();
	}

	/**
	 * Getter for height
	 * @return height
	 */
	public float getHeight()
	{
		return height;
	}

	/**
	 * Setter for height
	 * @param height
	 */
	public void setHeight(float height)
	{
		this.height = height;
		updateRenderer();
	}

	/**
	 * Getter for the renderer
	 * @return the renderer
	 */
	public Rectangle getRenderer()
	{
		return renderer;
	}

	/**
	 * Setter for the renderer
	 * @param renderer
	 */
	public void setRenderer(Rectangle renderer)
	{
		if(renderer != null)
		{
			this.renderer = renderer;
			updateRenderer();
		}
	}
}
