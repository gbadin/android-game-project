package compproject.onebuttongame;

import android.app.Activity;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends Activity
{

	public Thread GameLoop;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	        
		 // requesting to turn the title OFF
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // making it full screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
        GameMain instance = GameMain.getInstance();
        instance.init(this);
        setContentView(instance.getGlSurfaceView());
        
        GameLoop = new Thread("GameLoop")
        {
        	public void run()
        	{
        		GameMain.getInstance().run();
        	}
        };
        GameLoop.start();
	}
	
	@Override
	protected void onRestart()
	{
		super.onRestart();
		GameMain.getInstance().getGlSurfaceView().onResume();
		GameMain.getInstance().resume();
	}

	@Override
	protected void onStop()
	{
		super.onStop();
		GameMain.getInstance().getGlSurfaceView().onPause();
		GameMain.getInstance().pause();
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		GameMain.getInstance().getGlSurfaceView().onResume();
		GameMain.getInstance().resume();
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		GameMain.getInstance().getGlSurfaceView().onPause();
		GameMain.getInstance().pause();
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		GameMain.getInstance().stopRunning();
	}
	
	
	
	public void onAudioFocusChange(int focusChange) {
	    switch (focusChange) {
	        case AudioManager.AUDIOFOCUS_GAIN:
	            // resume playback
	        	GameMain.stateChangeMusic("resume");
	            break;

	        case AudioManager.AUDIOFOCUS_LOSS:
	            // Lost focus for an unbounded amount of time: stop playback and release media player
	            GameMain.stateChangeMusic("stahp");
	            break;

	        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
	            // Lost focus for a short time, but we have to stop
	            // playback. We don't release the media player because playback
	            // is likely to resume
	        	GameMain.stateChangeMusic("pause");
	            break;

	        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
	            // Lost focus for a short time, but it's ok to keep playing
	            // at an attenuated level
	        	GameMain.stateChangeMusic("duck");
	            break;
	    }
	}
}
