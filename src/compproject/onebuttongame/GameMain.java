package compproject.onebuttongame;

import java.util.HashMap;

import android.content.Context;
import android.media.MediaPlayer;
import android.opengl.GLSurfaceView;
import android.util.Log;

/**
 * 
 * @author Gr�goire
 *
 * A singleton class used for the base of the game.
 */
public class GameMain
{
	
	////////////////////////////////
	//STATIC METHODS AND ATTRIBUTES
	////////////////////////////////
	
	/**
	 * the unique instance of the class
	 */
	private static GameMain instance;
	
	/**
	 * Function to get the current instance of the game.
	 * Create a new instance if not exists
	 * @return current instance of the game
	 */
	public static GameMain getInstance()
	{
		if(instance == null)
		{
			instance =  new GameMain();
		}
		return GameMain.instance;
	}
	
	/**
	 * Function to delete the current instance of the game.
	 */
	public static void deleteInstance()
	{
		releaseMusic();
		instance = null;
	}
	
	/**
	 * Function to create a new instance of the game. (deleting the previous one)
	 * @return new instance of the game
	 */
	public static GameMain resetInstance()
	{
		releaseMusic();
		instance = new GameMain();
		return instance;
	}
	
	public static boolean musicPlaying()
	{
		return instance.mainMusic.isPlaying();
	}
	
	/**
	 * Function to change the state of music
	 * @param newState
	 */
	public static void stateChangeMusic(String newState)
	{
		if(newState == "resume")
		{
            if (!instance.mainMusic.isPlaying()) 
            	instance.mainMusic.start();
			instance.mainMusic.setVolume(1.0f, 1.0f);
		}
		
		if(newState == "duck")
		{
			instance.mainMusic.setVolume(0.1f, 0.1f);
		}
		
		if(newState == "pause")
		{
			if (instance.mainMusic.isPlaying())
				instance.mainMusic.pause();
		}
		
		if(newState == "stahp")
		{
			releaseMusic();
		}
	}
	
	public static void releaseMusic()
	{
		if (instance.mainMusic.isPlaying()) instance.mainMusic.stop();
		instance.mainMusic.release();
		instance.mainMusic = null;
	}
	
	/**
	 * Create a log for debug with "oneButtonGame" for tag
	 * @param log a string for the text of the log
	 */
	public static void log(String log)
	{
		Log.i("oneButtonGame", log);
	}
	
	///////////////////////////////
	//MEMBER ATTRIBUTES AND METHODS
	///////////////////////////////
	
	private String currentState;
	private HashMap<String, GameState> states;
	private GlRenderer renderer;
	/** The OpenGL view */
	private CustomGLSurfaceView glSurfaceView;
	private boolean isInited;
	private long timeElapsed;
	private Context context;
	private long currentTime;
	private long lastTime;
	private boolean firstDraw;
	private boolean running;
	private boolean pause;
	private MediaPlayer mainMusic;
	private int windowWidth;
	private int windowHeight;
	
	private GameMain()
	{isInited = false;}
	
	/**
	 * Init the Game main with the context so it can be accessed
	 * from  everywhere the context is the mainActivity
	 * @param context the context of the application
	 */
	public void init(Context context)
	{
		if (context != null)
		{
			this.context = context;
	        // Initiate the Open GL view and
	        // create an instance with this activity
	        glSurfaceView = new CustomGLSurfaceView(context);
	        
	        windowWidth = context.getResources().getDisplayMetrics().widthPixels;
	        windowHeight = context.getResources().getDisplayMetrics().heightPixels;
	        // set our renderer to be the main renderer with
	        // the current activity context
	        renderer = new GlRenderer(context,windowWidth,windowHeight);
	        glSurfaceView.setRenderer(renderer);

	        //create the state list
			states = new HashMap<String, GameState>();
			//create the first state
			states.put("menu", new MenuState());
			states.put("flip", new FlipGameState());
			states.put("splashscreen", new SplashScreenState());
			states.put("gameover", new GameOverState());
			states.put("control", new ControlState());
			states.put("credit", new CreditState());
			states.put("win", new WinState());
			currentState = "splashscreen";
			
			mainMusic = MediaPlayer.create(context, R.raw.alightinthedarknessof);
			
			isInited = true;
			firstDraw = false;
			pause = false;
		}
	}
	
	/**
	 * 
	 * @return the renderer
	 */
	public GlRenderer getRenderer()
	{
		return renderer;
	}
	
	/**
	 * 
	 * @return the GLSurfaceView
	 */
	public GLSurfaceView getGlSurfaceView()
	{
		return glSurfaceView;
	}

	/**
	 * get the stqte with it's name
	 * @param stateName the name of the state
	 * @return the state
	 */
	public GameState getState(String stateName)
	{
		return states.get(stateName);
	}
	
	/**
	 * 
	 * @return the name of the current state
	 */
	public String getCurrentStateName()
	{
		return currentState;
	}
	
	/**
	 * 
	 * @return the current state
	 */
	public GameState getCurrentState()
	{
		return states.get(currentState);
	}
	
	public int getWindowWidth()
	{
		return windowWidth;
	}
	
	public int getWindowHeight()
	{
		return windowHeight;
	}
	
	/**
	 * 
	 * @return the time elapsed between the lat two updates
	 */
	public long getTimeElapsed()
	{
		return timeElapsed;
	}

	/**
	 * 
	 * @return the context
	 */
	public Context getContext()
	{
		return context;
	}

	/**
	 * 
	 * @param context the context
	 */
	public void setContext(Context context)
	{
		if(context != null)
			this.context = context;
	}

	/**
	 * 
	 * @return the current time
	 */
	public long getCurrentTime()
	{
		return currentTime;
	}

	/**
	 * 
	 * @return the time of the last update
	 */
	public long getLastTime()
	{
		return lastTime;
	}

	/**
	 * 
	 * @return true if running false otherwise
	 */
	public boolean isRunning()
	{
		return states.get(currentState) != null;
	}
	
	/**
	 * set running to false
	 */
	public void stopRunning()
	{
		running = false;
	}
	
	/**
	 * used to know if the was a draw (to not start update physics before we can see anything)
	 */
	public void haveBeenDrawed()
	{
		firstDraw = true;
	}
	
	/**
	 * pause the loop
	 */
	public void pause()
	{
		pause = true;
	}
	
	/**
	 * resume the loop
	 */
	public void resume()
	{
		pause = false;
		lastTime = System.currentTimeMillis();
	}
	
	public void changeMusic(String musicName)
	{
		
	}
	
	/**
	 * the main loop of the game
	 */
	public void run()
	{
		if(this.isInited)
		{
			//wait the other thread
			while(firstDraw == false)
			{
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			GameState currState = getCurrentState();
			
			currState.OnEntry();
			lastTime = System.currentTimeMillis();
			
			running = true;
			while(currentState != "nothing" && running == true)
			{
				//get the time elapsed in ms
				currentTime = System.currentTimeMillis();
				timeElapsed = (currentTime - lastTime);
				lastTime = currentTime;
				
				if (mainMusic == null)
					mainMusic = MediaPlayer.create(context, R.raw.alightinthedarknessof);
				
				if(!pause)
				{
					if(!mainMusic.isPlaying())
						mainMusic.start();
					//update the current state
					currState.updtateState(timeElapsed);
					
					//check if the state have ended and if true, what is the next state
					if(currState.isStateEnded())
					{
						//we do the exist action of the previous state
						currState.OnExit();
						currentState = currState.nextState();
						currState = states.get(currentState);
						//if there is a state, we do the entry action.
						if(currState != null)
						{
							currState.OnEntry();
						}
						//if the state is null, we quit the application y setting the current state string to nothing. 
						else
						{
							currentState = "nothing";
						}
					}
				}
				else
				{
					try {
						if(mainMusic != null)
							releaseMusic();
						Thread.sleep(50);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
}
