package compproject.onebuttongame;

import java.util.HashSet;
import java.util.Iterator;

import javax.microedition.khronos.opengles.GL10;

import android.view.MotionEvent;

public class SplashScreenState extends GameState {

	private double timer;
	
	public SplashScreenState()
	{
		renderList = new HashSet<RenderNode>();
		Rectangle tmp = new Rectangle(0.0f,0.0f,2.0f,2.0f, RenderNode.ALL_CENTERED);
		tmp.setTextureID(R.drawable.splash_screen);
		renderList.add(tmp);
		nextState = "";
	}
	
	@Override
	public void OnEntry()
	{
		//on entry, we init the timer at 3000 ms (3s)
		timer = 3000;
		nextState = "";
	}
	

	@Override
	public void updtateState(long timeElapsed)
	{
		timer -= timeElapsed;
		if(timer <= 0)
			nextState = "menu";
	}

	@Override
	public void OnExit()
	{
		
	}
	
	@Override
	public void draw(GlRenderer renderer, GL10 gl)
	{
		
		renderer.initLayer(0.0f,0.0f,2.0f);
		Iterator<RenderNode> it = renderList.iterator();
		while(it.hasNext())
		{
			renderer.resetModelView(gl);
			it.next().draw(gl);
		}
	}

	@Override
	public void handleTouchingEvents(MotionEvent event)
	{
		nextState = "menu";
	}

}
