package compproject.onebuttongame;

import javax.microedition.khronos.opengles.GL10;

public interface RenderNode {
	/*
	 * Options for the renderNodes
	 */
	public static final int NOT_CENTERED = 0; //0b000
	public static final int X_CENTERED = 1; //0b001
	public static final int Y_CENTERED = 2; //0b010
	public static final int ALL_CENTERED = 3; //0b011
	/*
	 * options for the labels 
	 */
	public static final int DIMENSION_HEIGHT = 0; //0b000
	public static final int DIMENSION_WIDTH = 4; //0b100

	public void draw(GL10 gl);
}
