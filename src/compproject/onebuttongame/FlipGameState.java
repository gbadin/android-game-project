package compproject.onebuttongame;

import java.util.HashSet;
import java.util.Iterator;

import javax.microedition.khronos.opengles.GL10;

import android.view.MotionEvent;

public class FlipGameState extends GameState {

	private Map map;
	private int mapID;
	private float xCamera;
	private byte gravity;
	private float xSpeed;
	private float ySpeed;
	private float lastX;
	private float lastY;
	private boolean noUpdate;
	private boolean drawing;
	private boolean loading;
	
	/**
	 * 
	 */
	public FlipGameState ()
	{
		renderList = new HashSet<RenderNode>();
		ySpeed = 1.0f;
		xSpeed = 1.0f;
		loading = false;
		drawing = false;
	}
	
	/**
	 * 
	 * @param mapID
	 */
	public void LoadMap(int mapID)
	{
		while(drawing){}
		loading = true;
		this.mapID = mapID;
		map = new Map();
		map.LoadFromFile("level"+mapID+".dat");
		if(map.isLoaded())
		{
			Iterator<InteractiveObject> it = map.getObstacles().iterator();
			while(it.hasNext())
			{
				renderList.add(it.next().getRenderer());
			}
		
			xCamera = map.getPlayer().getX();
		}
		else
		{
			nextState = "win";
		}
		loading = false;
	}
	
	@Override
	public void handleTouchingEvents(MotionEvent event)
	{
		if(event.getAction() == MotionEvent.ACTION_DOWN)
			gravity *= -1;
	}
	
	@Override
	public void OnEntry()
	{
		nextState ="";
		noUpdate = true;
		LoadMap(0);
		gravity = -1;
	}

	@Override
	public void updtateState(long timeElapsed)
	{
		if(noUpdate)//prevent the player to move when the state have not been displayed yet
		{
			timeElapsed =  0;
		}
		//refresh rate really to slow, so we see the square move frame per frame
		lastX = map.getPlayer().getX();
		lastY =  map.getPlayer().getY();
				
		map.getPlayer().setY( map.getPlayer().getY()+(float)gravity*(ySpeed*timeElapsed/1000.0f));
		map.getPlayer().setX( map.getPlayer().getX()+ySpeed*timeElapsed/1000.0f);
		xCamera += xSpeed*timeElapsed/1000.0f;
		
		for(InteractiveObject obstacle :  map.getObstacles())
		{
			if(map.getPlayer().isCollision(obstacle))
			{
				//place the object at the top, the bottom of the other depending on the gravity or on the left
				if(lastX+map.getPlayer().getWidth() <= obstacle.getX()) //&& lastY+obstacle.getHeight() > obstacle.getY() && lastY < obstacle.getY()+obstacle.getHeight())//test if the last position was at the left of the obstacle
				{
					map.getPlayer().setX(obstacle.getX()-map.getPlayer().getWidth());//left
				}
				else if(gravity == 1)
				{
					map.getPlayer().setY(obstacle.getY()-map.getPlayer().getHeight()-0.001f);//bottom
				}
				else
				{
					map.getPlayer().setY(obstacle.getY()+obstacle.getHeight()+0.001f);//top
				}
			}
		}
		if(map.getPlayer().isCollision(map.getEnd()))
		{
			LoadMap(mapID+1);
		}
		else if(map.getPlayer().getX()+ map.getPlayer().getWidth() < xCamera-1.0 || map.getPlayer().getY() + map.getPlayer().getHeight() < -1 || map.getPlayer().getY() > 1)
		{
			nextState = "gameover";
		}
	}

	@Override
	public void OnExit()
	{
	}

	@Override
	public void draw(GlRenderer renderer, GL10 gl)
	{
		if(map != null && map.isLoaded())
		{
			while(loading){}
			drawing = true;
			try
			{
				noUpdate = false;
				float positioninX = xCamera;
				if(positioninX < 0.0f)
				{
					positioninX = 0.0f;
				}
				renderer.initLayer(positioninX,0.0f,2.0f);
				
				renderer.resetModelView(gl);
				map.getEnd().getRenderer().draw(gl);
				
				Iterator<RenderNode> it = renderList.iterator();
				while(it.hasNext())
				{
					renderer.resetModelView(gl);
					it.next().draw(gl);
				}
				
				renderer.resetModelView(gl);
				map.getPlayer().getRenderer().draw(gl);
			}
			catch(NullPointerException e)
			{
				e.printStackTrace();
			}
			drawing = false;
		}
	}

}
