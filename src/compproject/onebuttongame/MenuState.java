package compproject.onebuttongame;

import javax.microedition.khronos.opengles.GL10;

import android.view.MotionEvent;

public class MenuState extends GameState
{
	private Rectangle background;
	private InteractiveObject playButton;
	private InteractiveObject controlButton;
	private InteractiveObject creditButton;
	private int selectButton;
	
	public MenuState()
	{
		background = new Rectangle(-1.0f,-1.0f,2.0f,2.0f);
		background.setTextureID(R.drawable.bg);
		playButton = new InteractiveObject(-0.3f,0.1f,0.6f,0.3f);
		playButton.getRenderer().setTextureID(R.drawable.play);
		controlButton = new InteractiveObject(-0.3f,-0.3f,0.6f,0.3f);
		controlButton.getRenderer().setTextureID(R.drawable.control);
		creditButton = new InteractiveObject(-0.3f,-0.7f,0.6f,0.3f);
		creditButton.getRenderer().setTextureID(R.drawable.credits);
		selectButton = 0;
	}
	
	@Override
	public void OnEntry() {
		// TODO Auto-generated method stub
		nextState ="";
	}

	@Override
	public void updtateState(long timeElapsed) {
		// TODO Auto-generated method stub

	}

	@Override
	public void OnExit() {
		// TODO Auto-generated method stub

	}

	@Override
	public void draw(GlRenderer renderer, GL10 gl) {
		renderer.initLayer(0.0f,0.0f,2.0f);
		renderer.resetModelView(gl);
		background.draw(gl);
		renderer.resetModelView(gl);
		playButton.getRenderer().draw(gl);
		renderer.resetModelView(gl);
		controlButton.getRenderer().draw(gl);
		renderer.resetModelView(gl);
		creditButton.getRenderer().draw(gl);
	}

	@Override
	public void handleTouchingEvents(MotionEvent event) {
		int wWidth = GameMain.getInstance().getWindowWidth();
		float wScaleW = GameMain.getInstance().getRenderer().getScaleWidth();
		int wHeight = GameMain.getInstance().getWindowHeight();
		float wScaleH = GameMain.getInstance().getRenderer().getScaleHeight();
		
		float xTouchPos = ((event.getX()/wWidth)*2 - 1)/wScaleW;
		float yTouchPos = ((event.getY()/wHeight)*2 - 1)/-wScaleH;
		
		if(event.getAction() == MotionEvent.ACTION_DOWN)
		{
			if(xTouchPos >= playButton.getX() && xTouchPos <= playButton.getX()+playButton.getWidth())
			{
				if(yTouchPos >= playButton.getY() && yTouchPos <= playButton.getY()+playButton.getHeight())
				{
					playButton.getRenderer().setTextureID(R.drawable.play_touch);
					selectButton = 1;
				}
				if(yTouchPos >= controlButton.getY() && yTouchPos <= controlButton.getY()+controlButton.getHeight())
				{
					controlButton.getRenderer().setTextureID(R.drawable.control_touched);
					selectButton = 2;
				}
				if(yTouchPos >= creditButton.getY() && yTouchPos <= creditButton.getY()+creditButton.getHeight())
				{
					creditButton.getRenderer().setTextureID(R.drawable.credits_touch);
					selectButton = 3;
				}
			}
		}
		
		if(event.getAction() == MotionEvent.ACTION_UP)
		{
				if(selectButton == 1)
				{
					playButton.getRenderer().setTextureID(R.drawable.play);
					if(xTouchPos >= playButton.getX() && xTouchPos <= playButton.getX()+playButton.getWidth())
					{
						if(yTouchPos >= playButton.getY() && yTouchPos <= playButton.getY()+playButton.getHeight()){
							nextState = "flip";}
					}
				}
				if(selectButton == 2)
				{
					controlButton.getRenderer().setTextureID(R.drawable.control);
					if(xTouchPos >= controlButton.getX() && xTouchPos <= controlButton.getX()+controlButton.getWidth())
					{
						if(yTouchPos >= controlButton.getY() && yTouchPos <= controlButton.getY()+controlButton.getHeight()){
							nextState = "control";}
					}
				}
				if(selectButton == 3)
				{
					creditButton.getRenderer().setTextureID(R.drawable.credits);
					if(xTouchPos >= creditButton.getX() && xTouchPos <= creditButton.getX()+creditButton.getWidth())
					{
						if(yTouchPos >= creditButton.getY() && yTouchPos <= creditButton.getY()+creditButton.getHeight()){
							nextState = "credit";}
					}
				}
				
				selectButton = 0;
		}
	}

}
