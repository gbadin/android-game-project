package compproject.onebuttongame;

import javax.microedition.khronos.opengles.GL10;

import android.view.MotionEvent;

public class WinState extends GameState {

private Rectangle background;
	
	public WinState()
	{
		background = new Rectangle(-1.0f,-1.0f,2.0f,2.0f);
		background.setTextureID(R.drawable.winscreen);
	}
	
	@Override
	public void OnEntry() {
		// TODO Auto-generated method stub
		nextState = "";
	}

	@Override
	public void updtateState(long timeElapsed) {
		// TODO Auto-generated method stub

	}

	@Override
	public void OnExit() {
		// TODO Auto-generated method stub

	}

	@Override
	public void draw(GlRenderer renderer, GL10 gl) {
		// TODO Auto-generated method stub
		renderer.initLayer(0.0f,0.0f,2.0f);
		renderer.resetModelView(gl);
		background.draw(gl);
	}

	@Override
	public void handleTouchingEvents(MotionEvent event) {
		// TODO Auto-generated method stub
		if(event.getAction() == MotionEvent.ACTION_DOWN)
			nextState = "menu";
	}

}
