package compproject.onebuttongame;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

import compproject.onebuttongame.R;

import android.content.Context;
import android.opengl.GLSurfaceView.Renderer;

public class GlRenderer implements Renderer
{

	private Rectangle border1, border2;
	private float scaleWidth;
	private float scaleHeight;
	
	private float xCenter,yCenter,scale;
	
	private Context context;

	public Context getContext()
	{
		return context;
	}


	/** Constructor to set the handed over context */
	public GlRenderer(Context context, int width, int height)
	{
		this.context = context;
		
		scaleWidth = scaleHeight = 1.0f;
		
		//case when the screen has a larger width (tablet, computer)
		if(width > height)
		{
			scaleWidth = (float)height/(float)width;
			border1 = new Rectangle(-1.0f,-1.0f,1-scaleWidth,2.0f);
			border2 = new Rectangle(scaleWidth,-1.0f,1-scaleWidth,2.0f);
		}
		//case when the screen has a larger height (phone)
		else if (height > width)
		{
			scaleHeight = (float)width/(float)height;
			border1 = new Rectangle(-1.0f,-1.0f,2.0f,1-scaleHeight);
			border2 = new Rectangle(-1.0f,scaleHeight,2.0f,1-scaleHeight);
		}
		//Log.i("oneButtonGame","scaleWidth : "+scaleWidth+" scaleHeight : "+scaleHeight);
	}


	@Override
	public void onDrawFrame(GL10 gl) {
		// clear Screen
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		GameMain.getInstance().getCurrentState().draw(this, gl);
		GameMain.getInstance().haveBeenDrawed();
		//draw two rectangle on the border of the screen.
		gl.glLoadIdentity();
		border1.draw(gl);
		gl.glLoadIdentity();
		border2.draw(gl);
	}
	
	public void initLayer(float xCenter, float yCenter, float scale) {
		this.xCenter = xCenter;
		this.yCenter = yCenter;
		this.scale = scale;
	}
	
	public void resetModelView(GL10 gl)
	{
		// Reset the Modelview Matrix
		gl.glLoadIdentity();
		//to create a square on the screen.
		gl.glScalef(scaleWidth, scaleHeight, 1.0f);
		gl.glScalef(1/(scale/2), 1/(scale/2), 1.0f);
		gl.glTranslatef(-xCenter,-yCenter, 0.0f);
	}
	
	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		if(height == 0) { 						//Prevent A Divide By Zero By
			height = 1; 						//Making Height Equal One
		}
 
		gl.glViewport(0, 0, width, height); 	//Reset The Current Viewport
 
		gl.glMatrixMode(GL10.GL_MODELVIEW); 	//Select The Modelview Matrix
		gl.glLoadIdentity(); 					//Reset The Modelview Matrix
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		// Load the texture for the rectangles
		if(scaleWidth != 1.0f)
		{
			border1.loadGLTexture(gl, this.context, R.drawable.left_border);
			border2.loadGLTexture(gl, this.context, R.drawable.right_border);
		}
		else
		{
			border1.loadGLTexture(gl, this.context, R.drawable.bottom_border);
			border2.loadGLTexture(gl, this.context, R.drawable.top_border);
		}

		gl.glEnable(GL10.GL_TEXTURE_2D);			//Enable Texture Mapping
		gl.glShadeModel(GL10.GL_SMOOTH); 			//Enable Smooth Shading
		gl.glClearColor(0.5f, 0.5f, 0.5f, 1.0f); 	//gray Background
		

		//Really Nice Perspective Calculations
		gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);
	}
	
	public float getScaleWidth() {
		return scaleWidth;
	}


	public float getScaleHeight() {
		return scaleHeight;
	}



}
