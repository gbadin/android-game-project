package compproject.onebuttongame;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Map {
	
	private ArrayList<InteractiveObject> obstacles;
	private InteractiveObject player;
	private InteractiveObject end;
	private float width, height;
	private boolean loaded;
	
	public ArrayList<InteractiveObject> getObstacles()
	{
		return obstacles;
	}

	public InteractiveObject getPlayer()
	{
		return player;
	}

	public InteractiveObject getEnd()
	{
		return end;
	}

	public float getWidth()
	{
		return width;
	}

	public float getHeight()
	{
		return height;
	}
	
	public boolean isLoaded()
	{
		return loaded;
	}
	
	/**
	 * Load a map from a file.
	 * @param fileName Name of the file to load
	 */
	public void LoadFromFile(String fileName)
	{
		loaded = false;
		player = new InteractiveObject();
		end = new InteractiveObject();
		obstacles = new ArrayList<InteractiveObject>();
		try 
		{
			InputStream in = GameMain.getInstance().getContext().getAssets().open(fileName) ;
		    InputStreamReader isr = new InputStreamReader (in) ;
		    BufferedReader buffreader = new BufferedReader (isr) ;
		
		    String readString = buffreader.readLine ( ) ;
		    while ( readString != null )
		    {
		    	String line[];
		    	switch(readString.charAt(0))
		    	{
		    		case 'd'://dimension of the map
		    			line = readString.split("[ ]+",3);
		    			width = Float.parseFloat(line[1]);
		    			height = Float.parseFloat(line[2]);
		    			break;
		    		case 'o'://object such as player (starting pos and size) end and obstacles
		    			line = readString.split("[ ]+",6);
		    			InteractiveObject tempObject  = new InteractiveObject(Float.parseFloat(line[1]), Float.parseFloat(line[2]), Float.parseFloat(line[3]), Float.parseFloat(line[4]));
		    			int textureNumber = Integer.parseInt(line[5]);
		    			if(textureNumber != -1)//set the texture only if there is a texture
		    			{
		    				switch(textureNumber)
		    				{
		    					case 0:
		    						tempObject.getRenderer().setTextureID(R.drawable.texture0);
		    						break;
		    					case 1:
		    						tempObject.getRenderer().setTextureID(R.drawable.texture1);
		    						break;
		    					case 2:
		    						tempObject.getRenderer().setTextureID(R.drawable.texture2);
		    						break;
		    					case 3:
		    						tempObject.getRenderer().setTextureID(R.drawable.texture3);
		    						break;
		    					case 4:
		    						tempObject.getRenderer().setTextureID(R.drawable.texture4);
		    						break;
		    					case 5:
		    						tempObject.getRenderer().setTextureID(R.drawable.texture5);
		    						break;
		    					case 6:
		    						tempObject.getRenderer().setTextureID(R.drawable.texture6);
		    						break;
		    					case 7:
		    						tempObject.getRenderer().setTextureID(R.drawable.texture7);
		    						break;
		    					case 8:
		    						tempObject.getRenderer().setTextureID(R.drawable.texture8);
		    						break;
		    					case 9:
		    						tempObject.getRenderer().setTextureID(R.drawable.texture9);
		    						break;
		    					case 10:
		    						tempObject.getRenderer().setTextureID(R.drawable.texture10);
		    						break;
	    						default:
	    							break;
		    				}
		    			}
		    			switch(readString.charAt(1))//assign to the right variable
		    			{
		    				case 'p':
		    					player = tempObject;
		    					break;
		    				case 'e':
		    					end = tempObject;
		    					break;
		    				case 'o':
		    					obstacles.add(tempObject);
		    					break;
		    			}
		    			break;
	    			default:
	    				break;
		    	}
		        readString = buffreader.readLine ( ) ;
		    }
		    loaded = true;
		    isr.close ( ) ;
		}
		catch ( IOException ioe )
		{
		    loaded = false;
		}
	}
}
