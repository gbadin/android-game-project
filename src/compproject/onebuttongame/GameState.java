package compproject.onebuttongame;

import java.util.HashSet;

import javax.microedition.khronos.opengles.GL10;

import android.view.MotionEvent;

public abstract class GameState {

	protected HashSet<RenderNode> renderList;
	protected String nextState;
	
	public abstract void OnEntry();

	public abstract void updtateState(long timeElapsed);

	public abstract void OnExit();

	public abstract void draw(GlRenderer renderer, GL10 gl);
	
	public abstract void handleTouchingEvents(MotionEvent event);
	
	public boolean isStateEnded()
	{
		return nextState != "";
	}
	
	public String nextState()
	{
		return nextState;
	}

}
